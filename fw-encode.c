#include <assert.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>

#define BASE_ADDR 0x3000u

static void encode_chunk(uint8_t out[8], uint8_t in[7])
{
    for (int i = 0; i < 7; ++i)
        out[i] = in[i] >> 1;

    unsigned lsbs = 0u;
    for (int i = 0; i < 7; ++i)
        lsbs |= (in[i] & 1) << i;
    out[7] = lsbs;
}

static void put_fourcc(const char a, const char b, const char c, const char d)
{
    putchar(a);
    putchar(b);
    putchar(c);
    putchar(d);
}

static void put_be_32(const uint_fast32_t x)
{
    putchar((x >> 24) & 0xFFu);
    putchar((x >> 16) & 0xFFu);
    putchar((x >> 8) & 0xFFu);
    putchar(x & 0xFFu);
}

static void put_be_24(const uint_fast32_t x)
{
    putchar((x >> 16) & 0xFFu);
    putchar((x >> 8) & 0xFFu);
    putchar(x & 0xFFu);
}

static void put_be_16(const uint_fast16_t x)
{
    putchar((x >> 8) & 0xFFu);
    putchar(x & 0xFFu);
}

static void put_vlq(uint_fast32_t x)
{
    if ((x >> 21) != 0u)
        putchar(0x80u | ((x >> 21) & 0x7Fu));

    if ((x >> 14) != 0u)
        putchar(0x80u | ((x >> 14) & 0x7Fu));

    if ((x >> 7) != 0u)
        putchar(0x80u | ((x >> 7) & 0x7Fu));

    putchar(x & 0x7Fu);
}

static void put_mthd(const unsigned format, const unsigned tracks, const unsigned division)
{
    put_fourcc('M', 'T', 'h', 'd');
    put_be_32(6);
    put_be_16(format);
    put_be_16(tracks);
    put_be_16(division);
}

static void put_time_signature(
    const int num, const int den_pow, const int clocks_per_tick, const int rate)
{
    putchar(0xFF);
    putchar(0x58);
    putchar(0x04);
    putchar(num);
    putchar(den_pow);
    putchar(clocks_per_tick);
    putchar(rate);
}

static void put_set_tempo(const long us_per_beat)
{
    putchar(0xFF);
    putchar(0x51);
    putchar(0x03);
    put_be_24(us_per_beat);
}

static void put_end_of_track()
{
    putchar(0xFF);
    putchar(0x2F);
    putchar(0);
}

static void put_note_on(const int channel, const int note, const int velocity)
{
    putchar(0x90 + channel);
    putchar(note);
    putchar(velocity);
}

static void put_note_off(const int channel, const int note, const int velocity)
{
    putchar(0x80 + channel);
    putchar(note);
    putchar(velocity);
}

static void put_begin_sysex(const size_t len)
{
    putchar(0xF0);
    put_vlq(len + 1);
}

static void put_end_sysex(void)
{
    putchar(0xF7);
}

static void put_waldorf_sysex(const int command, uint8_t* const data, const size_t len)
{
    put_begin_sysex(len + 6);

    unsigned checksum = 0u;
    putchar(0x3E); checksum += 0x3E;
    putchar(0x17); checksum += 0x17;
    putchar(0x7F); checksum += 0x7F;
    putchar(command); checksum += command;
    putchar(0); checksum += 0;
    for (size_t i = 0; i < len; ++i)
    {
        putchar(data[i]);
        checksum += data[i];
    }
    putchar((-checksum) & 0x7Fu);

    put_end_sysex();
}

int main()
{
    put_mthd(0, 1, 96);

    put_fourcc('M', 'T', 'r', 'k');
    const long track_length_offset = ftell(stdout);
    put_be_32(0);  // track length; fill in later

    put_vlq(0);
    put_time_signature(4, 2, 24, 8);
    put_vlq(0);
    put_set_tempo(416666);

    put_vlq(24);
    put_waldorf_sysex(0x77, NULL, 0);

    uint_fast16_t addr = BASE_ADDR;
    uint_fast8_t seq = 0u;
    while (!feof(stdin))
    {
        uint8_t buf[133];

        // read block
        const size_t block_size = fread(buf, 1, 128, stdin);
        if (ferror(stdin)) return 1;
        for (size_t i = block_size; i < 128; ++i)
            buf[i] = 0xFFu;
        const int next = getchar();
        const bool last = next == EOF;
        if (ferror(stdin)) return 1;
        ungetc(next, stdin);

        // compute checksum
        uint_fast32_t checksum = 0u;
        for (int i = 0; i < 128; i += 4)
        {
            checksum += buf[i];
            checksum += buf[i + 1] << 8;
            checksum += buf[i + 2] << 16;
            checksum += buf[i + 3] << 24;
        }
        buf[128] = checksum;
        buf[129] = checksum >> 8;
        buf[130] = checksum >> 16;
        buf[131] = checksum >> 24;
        buf[132] = 0xFFu;

        // encode block
        uint8_t sysex_buf[155];
        sysex_buf[0] = seq & 0x7Fu;
        sysex_buf[1] = (addr >> 14) & 0x7fu;
        sysex_buf[2] = (addr >> 7) & 0x7fu;
        assert((addr & 0x7Fu) == 0u);
        for (int i = 0; i < 19; ++i)
            encode_chunk(&sysex_buf[3 + i * 8], &buf[i * 7]);

        // write sysex
        put_vlq(24);
        put_waldorf_sysex(last ? 0x75 : 0x70, sysex_buf, sizeof sysex_buf);

        addr += 128;
        ++seq;
    }

    // TODO: fixup track length

    put_vlq(96);
    put_note_on(0, 64, 64);
    put_vlq(6);
    put_note_off(0, 64, 64);

    put_vlq(48);
    put_end_of_track();

    const long track_end_offset = ftell(stdout);
    fseek(stdout, track_length_offset, SEEK_SET);
    put_be_32(track_end_offset - track_length_offset - 4);

    return 0;
}

#include <inttypes.h>
#include <stdio.h>
#include <stdint.h>

static int offset = 0;

static int getvlq()
{
    int ret = 0;

    int c;
    do
    {
        c = getchar();
        if (c < 0) return -1;
        ++offset;
        ret <<= 7;
        ret += c & 0x7F;
    } while (c >= 0x80);

    return ret;
}

static int read_block(uint8_t block[7], unsigned* const message_sum)
{
    int c;
    for (int i = 0; i < 7; ++i)
    {
        c = getchar(); ++offset; *message_sum += c;
        block[i] = c << 1;
    }

    c = getchar(); ++offset; *message_sum += c;
    if (c < 0 || c >= 0x80) return -1;
    for (int i = 0; i < 7; ++i)
        block[i] |= (c >> i) & 1;

    return 0;
}


int main()
{
    // skip headers
    for (int i = 0; i < 0x25; ++i)
    {
        getchar(); ++offset;
    }

    uint8_t cur_seq = 0;
    uint32_t cur_addr = -1;

    while (!feof(stdin))
    {
        int c;

        if (getvlq() < 0) break;
        
        c = getchar(); ++offset;
        if (c < 0) break;
        if (c == 0x90) break;
        if (c != 0xF0)
        {
            fprintf(stderr, "bad sysex start byte at %i\n", offset - 1);
            return 1;
        }

        int len = getvlq();
        if (len < 0) break;
        if (len < 2)
        {
            fprintf(stderr, "bad length at %i\n", offset - 1);
            return 1;
        }
        len -= 2;  // mfr + sysex end

        unsigned message_sum = 0u;

        const int mfr = getchar(); ++offset;
        if (mfr < 0) break;
        if (mfr != 0x3E)
        {
            fprintf(stderr, "bad mfr code byte at %i\n", offset - 1);
            return 1;
        }
        message_sum += mfr;

        if (len == 160)
        {
            for (int i = 0; i < 4; ++i)
            {
                message_sum += getchar(); ++offset;
            }

            const int seq = getchar(); ++offset;
            if (seq != cur_seq)
            {
                fprintf(stderr, "bad sequence number %u at offset %i (expected %u)\n",
                    seq, offset - 1, cur_seq);
                return 1;
            }
            message_sum += seq;

            const int addr_h = getchar(); ++offset;
            message_sum += addr_h;
            const int addr_l = getchar(); ++offset;
            message_sum += addr_l;

            const uint32_t addr = (addr_h << 14) + (addr_l << 7);
            if (cur_addr == (uint32_t) -1)
            {
                cur_addr = addr;
                fprintf(stderr, "base addr %08X\n", cur_addr);
            }
            else
            {
                if (addr != cur_addr)
                {
                    fprintf(stderr, "address skipped from %"PRIX32" to %"PRIX32" at offset %i\n",
                        cur_addr, addr, offset - 1);
                    return 1;
                }
            }

            uint8_t data[133];

            for (int blk = 0; blk < 19; ++blk)
                read_block(data + blk*7, &message_sum);

            uint32_t payload_sum = 0u;
            for (int i = 0; i < 128; i += 4)
            {
                payload_sum += (uint32_t) data[i];
                payload_sum += (uint32_t) data[i+1] << 8;
                payload_sum += (uint32_t) data[i+2] << 16;
                payload_sum += (uint32_t) data[i+3] << 24;
            }

            const uint32_t expected_sum =
                (uint32_t) data[128] +
                ((uint32_t) data[129] << 8) +
                ((uint32_t) data[130] << 16) +
                ((uint32_t) data[131] << 24);
            if (payload_sum != expected_sum)
            {
                fprintf(stderr,
                    "bad payload checksum at offset %i computed %08"PRIX32" != expected %08"PRIX32"\n",
                    offset, payload_sum, expected_sum);
                return 1;
            }

            fwrite(data, 1, 128, stdout);

            // outer checksum
            message_sum += getchar(); ++offset;
            if ((message_sum & 0x7Fu) != 0u)
            {
                fprintf(stderr,
                    "bad message checksum at offset %i %u\n",
                    offset, message_sum & 0x7Fu);
                return 1; 
            }

            cur_seq = (cur_seq + 1u) & 0x7Fu;
            cur_addr += 128u;
        }
        else
        {
            for (int i = 0; i < len; ++i)
            {
                getchar(); ++offset;
            }
        }

        c = getchar(); ++offset;
        if (c < 0) break;
        if (c != 0xF7)
        {
            fprintf(stderr, "bad sysex end byte at %i\n", offset - 1);
            return 1;
        }
    }

    return 0;
}

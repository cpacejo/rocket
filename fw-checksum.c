#include <assert.h>
#include <inttypes.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>

#define CHECKSUM_OFFSET 0x20
#define END_OFFSET 0x24
#define FW_BASE 0x08003000u

static void put_le_32(const uint_fast32_t x, FILE* const s)
{
    fputc(x & 0xFFu, s);
    fputc((x >> 8) & 0xFFu, s);
    fputc((x >> 16) & 0xFFu, s);
    fputc((x >> 24) & 0xFFu, s);
}

int main(int argc, char* argv[])
{
    FILE* const file = argc > 1 ? fopen(argv[1], "r+") : NULL;
    if (file == NULL)
    {
        fprintf(stderr, "Usage: %s <firmware binary>\n", argv[0]);
        fprintf(stderr, "Updates the checksum of the specified firmware binary.\n");
        return 1;
    }

    uint32_t checksum = 0u;

    while (!feof(file))
    {
        const long offset = ftell(file);

        uint8_t buf[4];
        if (fread(buf, sizeof buf, 1, file) < 1)
        {
            if (feof(file)) break;
            return 1;
        }

        if (offset == CHECKSUM_OFFSET || offset == END_OFFSET)
            continue;

        checksum += buf[0];
        checksum += (uint32_t) buf[1] << 8;
        checksum += (uint32_t) buf[2] << 16;
        checksum += (uint32_t) buf[3] << 24;
    }

    const long length = ftell(file);
    assert(length >= 0);
    const uint32_t checksum_end = FW_BASE + length;
    checksum += checksum_end;

    fseek(file, CHECKSUM_OFFSET, SEEK_SET);
    put_le_32(-checksum, file);
    fseek(file, END_OFFSET, SEEK_SET);
    put_le_32(checksum_end, file);

    return 0;
}
